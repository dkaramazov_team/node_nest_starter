import { Get, Post, Res, Req, Controller } from '@nestjs/common';
import { ProductService } from './product.service';
import { Product } from './interfaces/product.interface';

@Controller()
export class ProductController {
    private products: Product[] = [];

    constructor(private productService: ProductService){}

    @Post('/product')
    create(@Req() req, @Res() res): void {
        console.log('name', req.body);
        const newProduct = {
            name: req.body.name
        }        
        this.productService.create(newProduct);        
        res.redirect('/product');
    }

    @Get('/product')
	product(@Res() res) {
        this.productService.findAll().then(data =>{
            this.products = data;
            res.render('home', {products: this.products});
        });
    }

    @Get('/product/add')
    addProduct(@Req() req, @Res() res) {
        res.render('product/add');
    }
}
