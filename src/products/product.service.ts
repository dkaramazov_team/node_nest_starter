import { Model } from 'mongoose';
import { Component } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { Product } from './interfaces/product.interface';
import { CreateProductDto } from './dto/product-create.dto';
import { ProductSchema } from './schemas/product.schema';

@Component()
export class ProductService {

  constructor(@InjectModel(ProductSchema) private readonly productModel: Model<Product>){}

  async create(createProductDto: CreateProductDto): Promise<Product> {
    const createdProduct = new this.productModel(createProductDto);
    return await createdProduct.save();
  }

  async findAll(): Promise<Product[]> {
    return await this.productModel.find().exec();
  }
}