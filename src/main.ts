import * as express from 'express';
import * as path from 'path';

import * as favicon from 'serve-favicon';
import * as exphbs from 'express-handlebars';
import * as bodyParser from 'body-parser';

import { NestFactory } from '@nestjs/core';
import { ApplicationModule } from './app.module';

async function bootstrap() {
	const app = await NestFactory.create(ApplicationModule);

	// static files
	app.use(express.static(path.join(__dirname, 'public')));
	app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))

	// view engine
	const hbs = exphbs.create({
		defaultLayout: 'main'
	});
	app.engine('handlebars', hbs.engine);
	app.set('view engine', 'handlebars');

	// body-parser
	app.use(bodyParser.urlencoded({extended: false}));
	app.use(bodyParser.json());

	await app.listen(3000);
}
bootstrap();
