import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose'

import { AppController } from './app.controller';
import { ProductModule } from './products/product.module';


@Module({
  imports: [ProductModule, MongooseModule.forRoot(`mongodb://${process.env.DB_DEV_USERNAME}:${process.env.DB_DEV_PASSWORD}@ds251845.mlab.com:51845/node_rest_api`)],
  controllers: [AppController],
  components: [],
})
export class ApplicationModule {}
