import { Get, Res, Controller } from '@nestjs/common';

@Controller()
export class AppController {
  @Get()
  root(@Res() res) {
    res.render('home', { message: 'Welcome to Nest.js' });
  }
}
